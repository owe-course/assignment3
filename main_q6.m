%{
%Filename: c:\Users\fabpi\modules\46211assignmentsolution\reports\Report3\2024\solution\matlab\classicalSolution\main_q5.m
%Path: c:\Users\fabpi\modules\46211assignmentsolution\reports\Report3\2024\solution\matlab\classicalSolution
%Created Date: Thursday, October 3rd 2024, 2:51:40 pm
%Author: Fabio Pierella
%
%Copyright (c) 2024 DTU Wind and Energy Systems
%}

% Question 6: add functions
addpath("functionsMat")
addpath("inputVariables")

waves5 = loadFromJSON("savedStates/waves4.json");
wind5 = loadFromJSON("savedStates/wind4.json");

%  Load the structures
monopileStruct = loadFromJSON("monopile.json");
iea22mw = loadFromJSON("iea22mw.json");
iea22mw.ARotor = iea22mw.DRotor^2*pi*0.25;


dtInt = 0.1;
tSpan = 660.;
tIntegration = 0.:dtInt:(tSpan-dtInt);
q0 = [1.; 0.];

q = ode4(@dqdt, tIntegration, q0, monopileStruct, ...
        iea22mw, waves5, wind5);

alpha = struct;
alpha.t = tIntegration;
alpha.alpha = q(:,1);
alpha.alphaDot = q(:,2);
saveToJSON(alpha, "savedStates/q.json")