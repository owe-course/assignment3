%{
%Filename: c:\Users\fabpi\modules\46211assignmentsolution\reports\Report3\2024\solution\matlab\classicalSolution\main_q5.m
%Path: c:\Users\fabpi\modules\46211assignmentsolution\reports\Report3\2024\solution\matlab\classicalSolution
%Created Date: Thursday, October 3rd 2024, 2:51:40 pm
%Author: Fabio Pierella
%
%Copyright (c) 2024 DTU Wind and Energy Systems
%}

% Question 7: add functions
addpath("functionsMat")
addpath("inputVariables")

% Load the response
q = loadFromJSON("savedStates/q.json");
q.alphaDotDot = gradient(q.alphaDot, q.t);

% Load the environment
waves = loadFromJSON("savedStates/waves4.json");
wind = loadFromJSON("savedStates/wind4.json");

%  Load the structures
monopileStruct = loadFromJSON("monopile.json");
iea22mw = loadFromJSON("iea22mw.json");
iea22mw.ARotor = iea22mw.DRotor^2*pi*0.25;

% Calculate the static wind loads
windDownsampled = downsample(wind, 2, {"t", "V_hub"});
windLoads = calculateStaticWindLoads(windDownsampled, iea22mw, ...
    monopileStruct, q);

wavesDownsampled = downsample(waves, 2, {"t", "u", "ut"});
wavesLoads = calculateStaticWaveLoads(wavesDownsampled, ...
        monopileStruct, q);

% Calculate the dynamic loads
monopileStruct = calculateElementwiseQuantities(monopileStruct);
dynamicLoads = calculateDynamicLoads(monopileStruct, q);

saveToJSON(dynamicLoads, "savedStates/dynLoads.json")

figure
plot(dynamicLoads.t, dynamicLoads.M, 'displayname', "MDyn")
plot(windLoads.t, windLoads.M, 'displayname', "Wind")
plot(wavesLoads.t, wavesLoads.M, 'displayname', "Waves")
legend on
