%{
%Filename: c:\Users\fabpi\OneDoc\Courses\46211_OffshoreWindEnergy\2024\Module3\Lectures\classicalSolution\Stud_\fft_example.m
%Path: c:\Users\fabpi\OneDoc\Courses\46211_OffshoreWindEnergy\2024\Module3\Lectures\classicalSolution\Stud_
%Created Date: Tuesday, October 8th 2024, 10:01:44 am
%Author: Fabio Pierella
%
%Copyright (c) 2024 DTU Wind and Energy Systems
%}

addpath("functionsMat")
addpath("inputVariables")

% Load the dictionaries from file
waves = loadFromJSON("wave1.json");
time = loadFromJSON("time.json");
waves = updateStruct(waves, time);
waves.t = 0:waves.dt:(waves.TDur-waves.dt);

% Calculate the spectrum
waves =calculateJONSWAPSpectrum(waves);

% Calculate the kinematics
randomSeedWaves = 1;
waves = generateRandomPhases(waves, randomSeedWaves);

% Slow eta
tic

etaSlow = zeros(length(waves.t),1);
for i_ = 1:length(waves.t)   
    for j_ = 1:length(waves.f)
        etaSlow(i_) = etaSlow(i_) + ...
            waves.amplitudeSpectrum(j_)*cos(2*pi*waves.f(j_)*waves.t(i_) + waves.randomPhases(j_) );
    end
end
fprintf('Elapsed time for etaSlow is: %.5f seconds.\n', toc);

% Fast eta
complexSpectrum = waves.amplitudeSpectrum.*exp(1j*waves.randomPhases);
newLength = (waves.TDur / waves.dt);
paddedSpectrum = pad2(complexSpectrum, newLength);

tic 
etaFast =newLength*real(ifft(paddedSpectrum));
fprintf('Elapsed time for etaFast is: %.5f seconds.\n', toc);

figure
plot(etaFast)
hold on
plot(etaSlow)

figure
plot(etaFast - etaSlow)