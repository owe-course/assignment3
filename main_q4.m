%{
%Filename: c:\Users\fabpi\OneDoc\Courses\46211_OffshoreWindEnergy\2024\Module3\Lectures\classical\main_q2.m
%Path: c:\Users\fabpi\OneDoc\Courses\46211_OffshoreWindEnergy\2024\Module3\Lectures\classical
%Created Date: Monday, September 30th 2024, 10:29:07 pm
%Author: Fabio Pierella
%
%Copyright (c) 2024 DTU Wind and Energy Systems
%}

% Question 3: add functions
addpath("../../../../../../src/assignmentcode/edition2024/matlab")
addpath("../../inputVariables")

iea22mw = loadFromJSON("iea22mw.json");
iea22mw.ARotor = iea22mw.DRotor^2*pi/4;

% Load the wind info
wind4 = loadFromJSON("wind4.json");
time4 = loadFromJSON("time.json");
wind4 = updateStruct(wind4, time4);

wind4.t = 0.:wind4.dt:(wind4.TDur - wind4.dt);
wind4 = calculateKaimalSpectrum(wind4);
randomSeedWind = 1;
wind4 = generateRandomPhases(wind4, randomSeedWind);
wind4 = calculateWindTimeSeries(wind4);

% Compute waves
waves4 = loadFromJSON("wave1.json");
time4 = loadFromJSON("time.json");
waves4 = updateStruct(waves4, time4);
waves4.t = 0:waves4.dt:(waves4.TDur-waves4.dt);
waves4 = calculateJONSWAPSpectrum(waves4);

randomSeedWaves = 2;
waves4 = generateRandomPhases(waves4, randomSeedWaves);
waves4 = calculateKinematics(waves4);
h = waves4.h;
% load the monopile
monopileStruct = loadFromJSON("monopile.json");

% Initialize the waves force Struct
waveForceQ4 = struct;
waveForceQ4.t = waves4.t;
waveForceQ4.F = zeros(length(waves4.t),1);
waveForceQ4.M = zeros(length(waves4.t),1);

for i_ = 1:length(waves4.t)
    % FIXME: call the forceIntegrate function to get the total wave force
    waveForceQ4.F(i_) = 0.; 
    waveForceQ4.M(i_) = 0.;
end

% Initialize the waves force Struct
windForceQ4 = struct;
windForceQ4.t = wind4.t;
windForceQ4.F = zeros(length(wind4.t),1);
windForceQ4.M = zeros(length(wind4.t),1);

for i_ = 1:length(wind4.t)
	% FIXME: call the F_wind function to get the wind force
    windForceQ4.F(i_) = 0.;
end

windForceQ4.M = windForceQ4.F*(monopileStruct.zBeamNodal(end)+h);

createFolderIfNotExists("savedStates")
saveToJSON(wind4, "savedStates/wind4.json")
saveToJSON(waves4, "savedStates/waves4.json")
