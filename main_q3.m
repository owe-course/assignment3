%{
%Filename: c:\Users\fabpi\OneDoc\Courses\46211_OffshoreWindEnergy\2024\Module3\Lectures\classical\main_q2.m
%Path: c:\Users\fabpi\OneDoc\Courses\46211_OffshoreWindEnergy\2024\Module3\Lectures\classical
%Created Date: Monday, September 30th 2024, 10:29:07 pm
%Author: Fabio Pierella
%
%Copyright (c) 2024 DTU Wind and Energy Systems
%}

addpath("functionsMat")

iea22mw = loadFromJSON("inputVariables/iea22mw.json");
iea22mw.ARotor = iea22mw.DRotor^2*pi/4;

% Load the wind info
wind3 = loadFromJSON("inputVariables/wind3.json");
timeQ3 = loadFromJSON("inputVariables/time.json");
wind3 = updateStruct(wind3, timeQ3);

wind3.t = 0.:wind3.dt:(wind3.TDur - wind3.dt);

wind3 = calculateKaimalSpectrum(wind3);
randomSeedWind = 100;
wind3 = generateRandomPhases(wind3, randomSeedWind);
wind3 = calculateWindTimeSeries(wind3);
