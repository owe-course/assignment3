%{
%Filename: c:\Users\fabpi\OneDrive - Danmarks Tekniske Universitet\Dokumenter\Courses\46211_OffshoreWindEnergy\2024\Module3\Code\classicalSolution\Stud_\main_q10.m
%Path: c:\Users\fabpi\OneDrive - Danmarks Tekniske Universitet\Dokumenter\Courses\46211_OffshoreWindEnergy\2024\Module3\Code\classicalSolution\Stud_
%Created Date: Saturday, October 19th 2024, 1:05:27 am
%Author: Fabio Pierella
%
%Copyright (c) 2024 DTU Wind and Energy Systems
%}

% Question 10: add functions
addpath("functionsMat")
addpath("inputVariables/")

% In this function, start from what you have in q9 and make a loop
% for all environmental conditions.

%  Load the structures
monopileStruct = loadFromJSON("monopile.json");
iea22mw = loadFromJSON("iea22mw.json");
iea22mw.ARotor = iea22mw.DRotor^2*pi*0.25;
iea22mw.VCutIn = 3.0; iea22mw.VCutOut = 25.0;

% time info
timeInfo = struct;
timeInfo.TDur = 660.;
timeInfo.dt = 0.05;
timeInfo.TTrans = 60.;
timeInfo.fHighCut = 0.5;

% Fatigue and scaling constants
mFatigue = 4.;
n_eq = 10.^7;

% Time factor
% FIXME: Calculate TLife and Tsim
TLife = 1.;
TSim = 1.;

% Rescale wind speed by taking into account shear factor 
% FIXME: include the correct scaleWind parameter using a shear factor of 1/7 and factor 2 on hub height
scaleWind = 1.;
table40 = readtable("table40.csv");
table40.V_10_scaled = table40.V_10 * scaleWind;

Filter = (table40.V_10_scaled > 3.) & ((table40.V_10_scaled < 25.));
table40 = table40(Filter, :);

% Initialize the M_eq to zero
table40.M_eq = 0.;

% Run the load calculations
for i_ = 1:length(table40.V_10)

    % Here, it is more conveniente to build the dictionaries on the fly
    % rather than reading them from file.
    
    tic
    wind = struct();
    wind.V_10 = table40.V_10_scaled(i_);
    wind.l = 340.2;
    wind.I = table40.I_norm(i_)/100;
    wind.randomSeed = (i_-1)*100 + 11;

    waves = struct();
    waves.Hs = table40.Hs(i_);
    waves.Tp = table40.Tp(i_);
    waves.gamma = table40.gamma_fat(i_);
    waves.h = 34.;
    waves.z = ensureColVec(-34:0);
    waves.randomSeed = (i_-1)*100 + 22;

    % FIXME: call the runSeaState function.
    % Then, remove transient and compute the fatigue for each sea state.

    % outputLoads = runEnvironmentalCondition(...)
   
    toc    

    % Rainflow count
    % FIXME: do the rainflow counting of fatigue here (see slides for example)
    rainflowCount = 0.; % apply the rainflow.count_cycles routine
    amplitude = 0.; % transform range into amplitude
    cycles = 0.; %
    cyclesUpscaled = cycles*TLife/TSim;

    % FIXME: calculate the equivalent moment here 
    M_eq = 0.;

    table40.M_eq(i_) = M_eq;

end