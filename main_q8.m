%{
%Filename: c:\Users\fabpi\modules\46211assignmentsolution\reports\Report3\2024\solution\matlab\classicalSolution\main_q8.m
%Path: c:\Users\fabpi\modules\46211assignmentsolution\reports\Report3\2024\solution\matlab\classicalSolution
%Created Date: Thursday, October 17th 2024, 3:25:16 pm
%Author: Fabio Pierella
%
%Copyright (c) 2024 DTU Wind and Energy Systems
%}

close all; clear all;

% Question 8: add functions
addpath("functionsMat")
addpath("inputVariables")

waves = loadFromJSON("wave1.json");
time = loadFromJSON("time.json");
waves = updateStruct(waves, time);

waves = calculateJONSWAPSpectrum(waves);
waves.t = 0:waves.dt:(waves.TDur-waves.dt);

randomSeedWaves = 1;
waves = generateRandomPhases(waves, randomSeedWaves);
waves = calculateFreeSurfaceElevationTimeSeries(waves);

tic
waves = calculateKinematics(waves);
waves = calculateFreeSurfaceElevationTimeSeries(waves);
toc 

tic
wavesFast = calculateKinematicsFFT(waves);
wavesFast = calculateFreeSurfaceElevationTimeSeriesFFT(wavesFast);
toc

% Make the wind part
iea22mw = loadFromJSON("iea22mw.json");
iea22mw.ARotor = iea22mw.DRotor^2*pi/4;

% Load the wind info
wind = loadFromJSON("wind4.json");
timeInfo = loadFromJSON("time.json");
wind = updateStruct(wind, timeInfo);

wind.t = 0.:wind.dt:(wind.TDur - wind.dt);
tic
wind = calculateWindTimeSeries(wind);
toc

tic
windFast = calculateWindTimeSeriesFFT(wind);
toc

% plot a comparison between the two
figure
plot(waves.t, waves.eta - wavesFast.eta, '.')

figure
plot(waves.t, waves.u(:,end) - wavesFast.u(:,end), '.')

figure
plot(wind.t, wind.V_hub - windFast.V_hub, '.')