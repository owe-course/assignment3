%{
%Filename: c:\Users\fabpi\OneDoc\Courses\46211_OffshoreWindEnergy\2024\Module3\Lectures\classical\main_q2.m
%Path: c:\Users\fabpi\OneDoc\Courses\46211_OffshoreWindEnergy\2024\Module3\Lectures\classical
%Created Date: Monday, September 30th 2024, 10:29:07 pm
%Author: Fabio Pierella
%
%Copyright (c) 2024 DTU Wind and Energy Systems
%}

addpath("functionsMat")

iea22mw = loadFromJSON("inputVariables/iea22mw.json");
iea22mw.ARotor = iea22mw.DRotor^2*pi/4;

thrust = struct();
thrust.V = (3.0:0.1:25.0)';
thrust.T = zeros(length(thrust.V),1);

for i_ = 1:length(thrust.V)
    thrust.T(i_) = F_wind(iea22mw, thrust.V(i_), thrust.V(i_), 0.);
end

plot(thrust.V, thrust.T)