%{
%Filename: c:\Users\fabpi\OneDrive - Danmarks Tekniske Universitet\Dokumenter\Courses\46211_OffshoreWindEnergy\2024\Module3\Code\classicalSolution\Stud_\main_q9.m
%Path: c:\Users\fabpi\OneDrive - Danmarks Tekniske Universitet\Dokumenter\Courses\46211_OffshoreWindEnergy\2024\Module3\Code\classicalSolution\Stud_
%Created Date: Friday, October 18th 2024, 12:48:59 pm
%Author: Fabio Pierella
%
%Copyright (c) 2024 DTU Wind and Energy Systems
%}

% Question 9: add functions
addpath("functionsMat")
addpath("inputVariables/")

% Load the rotor and the monopile which do not change
monopile = loadFromJSON("monopile.json");
iea22mw = loadFromJSON("iea22mw.json");
iea22mw.ARotor = iea22mw.DRotor^2*pi*0.25;
iea22mw.VCutIn = 3.0; iea22mw.VCutOut = 25.0;

% time information
timeInfo = struct;
timeInfo.TDur = 3660.;
timeInfo.dt = 0.05;
timeInfo.TTrans = 60.;
timeInfo.fHighCut = 0.5;

% Fatigue parameters
mFatigue = 4.;
n_eq = 10.^7;

% Time factor
% FIXME: Calculate TLife and Tsim
TLife = 1. ;
TSim = 1. ;

% Rescale wind speed by taking into account shear factor 
% FIXME: include the correct scaleWind parameter using a shear factor of 1/7 and factor 2 on hub height
scaleWind = 1.;

% Load the environmental conditions
wind = loadFromJSON("wind4.json");
waves = loadFromJSON("wave1.json");
wind.V_10 = wind.V_10*scaleWind;
wind.randomSeed = 1;
waves.randomSeed = 2;

% run the load calculation
outputLoads = runEnvironmentalCondition(wind, waves, iea22mw, monopile, timeInfo).total;

% Remove transient
Filter = outputLoads.t >= timeInfo.TTrans;
outputLoads.t = outputLoads.t(Filter);
outputLoads.F = outputLoads.F(Filter);
outputLoads.M = outputLoads.M(Filter);

% Rainflow count
% FIXME: do the rainflow counting of fatigue here (see slides for example)
rainflowCount = 0.; % apply the rainflow.count_cycles routine
amplitude = 0.; % transform range into amplitude
cycles = 0.; %
cyclesUpscaled = cycles*TLife/TSim;

% FIXME: calculate the equivalent moment here 
M_eq = 0.;