%{
%Filename: c:\Users\fabpi\OneDoc\Courses\46211_OffshoreWindEnergy\2024\Module3\Lectures\classical\main_q1.m
%Path: c:\Users\fabpi\OneDoc\Courses\46211_OffshoreWindEnergy\2024\Module3\Lectures\classical
%Created Date: Monday, September 30th 2024, 12:07:39 pm
%Author: Fabio Pierella
%
%Copyright (c) 2024 DTU Wind and Energy Systems
%}

% Question 1: add functions
addpath("functionsMat")
addpath("inputVariables/")

% Load the dictionaries from file
wavesQ1 = loadFromJSON("wave1.json");
timeQ1 = loadFromJSON("time.json");
wavesQ1 = updateStruct(wavesQ1, timeQ1);
wavesQ1.t = 0:wavesQ1.dt:(wavesQ1.TDur-wavesQ1.dt);

% Calculate the spectrum
wavesQ1 =calculateJONSWAPSpectrum(wavesQ1);

% Calculate the kinematics
randomSeedWaves = 1;
wavesQ1 = generateRandomPhases(wavesQ1, randomSeedWaves);
wavesQ1 = calculateFreeSurfaceElevationTimeSeries(wavesQ1);
wavesQ1 = calculateKinematics(wavesQ1);

% Load the monopile information and compute forces
monopileStruct = loadFromJSON("inputVariables/monopile.json");


% Initialize the force Struct
waveForceQ1 = struct;
waveForceQ1.t = wavesQ1.t;
waveForceQ1.F = zeros(length(wavesQ1.t),1);
waveForceQ1.M = zeros(length(wavesQ1.t),1);

for i_ = 1:length(wavesQ1.t)
    [waveForceQ1.F(i_), waveForceQ1.M(i_)] = forceIntegrate(monopileStruct, ...
        wavesQ1.u(i_,:), wavesQ1.ut(i_,:), wavesQ1.z, 0.);
end

% plot the free surface elevation
plot(wavesQ1.t, wavesQ1.eta)
grid on;
xlabel("t[s]"); ylabel("eta[m]")

figure
plot(waveForceQ1.t, waveForceQ1.F)
grid on;
xlabel("t[s]"); ylabel("F[N]")