import numpy as np

def calculateKaimalSpectrum(windDict):
    
    # Store it inside the wind dictionary
    outputDict = dict()
    outputDict.update(windDict)
    
    # Calculate frequency information
    df = windDict["TDur"]**-1
    f = np.arange(df, windDict["fHighCut"], df)

    # Calculate the Kaimal spectrum
    # FIXME: Program spectrum 
    Spectrum = np.ones_like(f)
    amplitudeSpectrum = np.sqrt(2*Spectrum*df)

    outputDict["Spectrum"] = Spectrum
    outputDict["amplitudeSpectrum"] = amplitudeSpectrum
    outputDict["f"] = f
    
    return outputDict

def calculateWindTimeSeries(windDict):
    t = windDict["t"]
    f = windDict["f"]
    windTimeSeries = np.zeros_like(t)
    
    for i_, _ in enumerate(t):
        for j_, _ in enumerate(f):
            # FIXME: add random phases
            windTimeSeries[i_] += windDict["amplitudeSpectrum"][j_]*np.cos(2*np.pi*f[j_]*t[i_] )
    
    # Store the result
    outputDict = dict()
    outputDict.update(windDict)
    outputDict["t"] = t
    outputDict["V_hub"] = windTimeSeries + windDict["V_10"]
    
    return outputDict


def calculateWindTimeSeriesFFT(windDict):
    t = windDict["t"]
    f = windDict["f"]
    windTimeSeries = np.zeros_like(t)
    
    M = len(t)
    # FIXME: compute the fft kernel and perform the IFFT
    windTimeSeriesKernel = np.zeros_like(t) # compute the freq. domain kernel and pad to M
    windTimeSeries = np.zeros_like(t) # perform the IFFT in this line
    
    # Store the result
    outputDict = dict()
    outputDict.update(windDict)
    outputDict["t"] = t
    outputDict["V_hub"] = windTimeSeries + windDict["V_10"]
    
    return outputDict