%{
%Filename: c:\\Users\\fabpi\\OneDoc\\Courses\\46211_OffshoreWindEnergy\\2024\\Module3\\Lectures\\classicalSolution\\Stud_\\ode_example.m
%Path: c:\\Users\\fabpi\\OneDoc\\Courses\\46211_OffshoreWindEnergy\\2024\\Module3\\Lectures\\classicalSolution\\Stud_
%Created Date: Tuesday, October 8th 2024, 9:21:54 am
%Author: Fabio Pierella
%
%Copyright (c) 2024 DTU Wind and Energy Systems
%}

addpath("functionsMat")

% Initial conditions
y0 = [1.0;0.];

% Generalized masses and stiffnesses
GM = 1.0;
GD = 0.1;
GK = 1.0;

% Generalized forcing
omega_f = 1.0;
A_f = 0.;
phi_f = 0.;

% Define GF and the dqdt
GF = @(t) A_f*sin(omega_f*t + phi_f);
dqdt = @(t,q) [q(2); (-GK*q(1) - GD*q(2) + GF(t))/GM];

tMax = 500.;
tspan = 0.:0.1:(tMax-0.1);

% Integration
y = ode4(dqdt, tspan, y0);

% plotting
figure
plot(tspan, y(:,1))
hold on
plot(tspan, y(:,2))
legend({"position","velocity"})
grid on
xlabel("t(s)")



