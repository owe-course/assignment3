function outputStruct = calculateKinematicsFFT(waveStruct)

    % Time information
    t = waveStruct.t;
    f = waveStruct.f;
    omega = f*2*pi;

    % Reference some variables for easier handling
    h = waveStruct.h;
    z = waveStruct.z;
    u = zeros(length(t), length(z));
    ut = zeros(length(t), length(z));
    a = waveStruct.amplitudeSpectrum;
    epsilon = waveStruct.randomPhases;

    g = loadConstants().g;
    k = dispersion(f, g, h);

    M = length(t);

    for iz = 1:length(z)
        
        % FIXME: compute the fft kernel and perform the IFFT
        uHat = zeros(length(t),1); % compute the freq. domain kernel and pad to M
        utHat = zeros(length(t),1); 

        u(:,iz) = zeros(length(t),1); % perform the IFFT in this line
        ut(:,iz) = zeros(length(t),1);
    end
    
    outputStruct = struct();
    outputStruct = updateStruct(outputStruct, waveStruct);
    outputStruct.t = waveStruct.t;
    outputStruct.u = u;
    outputStruct.ut = ut;
    
end