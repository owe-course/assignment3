function numbers = lcg(seed, n)
    % Linear congruential random number generator.
    % Chosen to have the sampe implementation in Matlab and Python.
    % See: https://en.wikipedia.org/wiki/Linear_congruential_generator

    a = 1103515245;
    c = 12345;
    m = 2^31;

    numbers = zeros(n,1); % column array by design
    numbers(1) = seed;
    for i = 2:n
        numbers(i) = mod(a * numbers(i-1) + c, m);
    end
    numbers = 2*pi*numbers / m; % Normalize to [0, 1]
end