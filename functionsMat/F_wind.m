function FwindOut = F_wind(rotorDict, V_10, V_hub, x_dot)

    FwindOut =F_avg(rotorDict, V_10) + ...
             fRed(rotorDict, V_10)* ...
             (F_var(rotorDict, V_hub, x_dot) - F_avg(rotorDict, V_10));

end 

function FAvgOut = F_avg(rotorDict, V_10)
    c = loadConstants();
    FAvgOut = 0.5*c.rho_air*rotorDict.ARotor*Ct(rotorDict, V_10)*V_10^2.0;
end


function FVarOut = F_var(rotorDict, V_hub, x_dot)


    c = loadConstants();
    V_rel = V_hub - x_dot;
    % FIXME: Implement the variable force
    FVarOut = 0.;

end

function fRedOut = fRed(rotorDict, V_10)
    if V_10 < rotorDict.VRated
        fRedOut = 0.54;
    else
        fRedOut = 0.54 + 0.027*(V_10 - rotorDict.VRated);
    end
end

function CtOut = Ct(rotorDict, V)
    % FIXME: Correct these functions according to wind model
    if (V <= rotorDict.V1) 
        CtOut = rotorDict.Ct0;
    elseif (V>rotorDict.V1 & V<=rotorDict.V2)                
        CtOut = rotorDict.Ct0;
    else
        CtOut = rotorDict.Ct1;
    end
end