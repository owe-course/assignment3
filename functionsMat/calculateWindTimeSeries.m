function outputStruct = calculateWindTimeSeries(windStruct)
    
    t = windStruct.t;
    f = windStruct.f;
    windTimeSeries = zeros(length(t),1);

    for i = 1:length(t)
        for j = 1:length(f)
            % FIXME: Add random phases to calculation
            windTimeSeries(i) = windTimeSeries(i) + ...
                windStruct.amplitudeSpectrum(j)*cos(2*pi*f(j)*t(i));
        end
    end

    outputStruct = struct();
    outputStruct = updateStruct(outputStruct, windStruct);
    outputStruct.t = t;
    % FIXME: Add the mean wind speed
    outputStruct.V_hub = windTimeSeries;

end
