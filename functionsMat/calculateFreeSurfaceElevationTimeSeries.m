function outputStruct = calculateFreeSurfaceElevationTimeSeries(inputStruct)
    
    t = inputStruct.t;
    f = inputStruct.f;
    
    omega = f*2*pi;
    freeSurfTimeSeries = zeros(length(t),1);

    for i = 1:length(t)
        for j = 1:length(f)
            % FIXME: Add random phases to calculation
            freeSurfTimeSeries(i) = freeSurfTimeSeries(i) + ...
                inputStruct.amplitudeSpectrum(j)*cos(omega(j)*t(i)  );
        end
    end

    outputStruct = struct();
    outputStruct = updateStruct(outputStruct, inputStruct);

    outputStruct.t = t;
    outputStruct.eta = freeSurfTimeSeries;

end