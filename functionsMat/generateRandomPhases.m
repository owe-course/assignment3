function outputStruct = generateRandomPhases(inputStruct, seed)

    % Custom random number generator
    % Basd on LCG random number generator
    % https://en.wikipedia.org/wiki/Linear_congruential_generator

    % Custom generator
    phi = lcg(seed, length(inputStruct.Spectrum));
    
    outputStruct = struct();
    outputStruct = updateStruct(outputStruct, inputStruct);
    outputStruct.randomPhases = phi;
end
