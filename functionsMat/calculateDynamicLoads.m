function outputStruct = calculateDynamicLoads(structure, q)

    % Create output dict
    outputStruct = struct;
    outputStruct.F = zeros(size(q.t));
    outputStruct.M = zeros(size(q.t));
    
    % some helper quantities
    dz = structure.dz;
    zElement = structure.zBeamElement;
    h = abs(structure.zBeamNodal(1));

    % calculations
    outputStruct.t = q.t;
    for i_ =1:length(q.t)
        % FIXME: insert formula for dynamic loads here
        outputStruct.F(i_) = 0.;
        outputStruct.M(i_) = 0.;
    end

end