function constants = loadConstants()
    
    constants = struct;
    constants.g = 9.81;
    constants.rho_air = 1.22;
    constants.rho_water = 1025.;
    constants.floatTolerance = 1.e-6;
end