function k = dispersion(f,g,h)

    N=length(f);
    omega=2*pi*f;
    k=zeros(size(f));
    myFun = @(k,omega,g,h) omega^2-g*k*tanh(k*h); % modify dispersion relation to work for all depths
    kGuess=omega(1)/sqrt(g*h); % we use shallow water limit as guess for first k
    for j=1:N
        k(j) = fzero(@(x) myFun(x,omega(j),g,h) , kGuess);
        kGuess = k(j);         % we use solution for neighbooring frequency for all others 
    end

end