function outputStruct = calculateKinematics(waveStruct)

    % Time information
    t = waveStruct.t;
    f = waveStruct.f;
    omega = f*2*pi;

    % Reference some variables for easier handling
    h = waveStruct.h;
    z = waveStruct.z;
    u = zeros(length(t), length(z));
    ut = zeros(length(t), length(z));
    a = waveStruct.amplitudeSpectrum;
    epsilon = waveStruct.randomPhases;

    g = loadConstants().g;
    k = dispersion(f, g, h);

    for it = 1:length(t)
        for iz = 1:length(z)
            % FIXME: add back the correct expressions for these terms
            u(it,iz)= sum(a.*cos( omega*t(it)+epsilon) );
            ut(it,iz)=sum(-a.*omega.*omega.*sin( omega*t(it)+epsilon) );
        end
    end
    
    outputStruct = struct();
    outputStruct = updateStruct(outputStruct, waveStruct);
    outputStruct.t = waveStruct.t;
    outputStruct.u = u;
    outputStruct.ut = ut;
    
end