
function mergedStruct = updateStruct(oldStruct, newStruct)

    fields = fieldnames(newStruct);
    mergedStruct = oldStruct;
    
    for i = 1:numel(fields)
        mergedStruct.(fields{i}) = newStruct.(fields{i});    
    end

end