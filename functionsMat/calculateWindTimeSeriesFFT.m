function outputStruct = calculateWindTimeSeriesFFT(windStruct)
    
    t = windStruct.t;
    f = windStruct.f;
    windTimeSeries = zeros(length(t),1);

    M = length(t);
    windHat = zeros(length(t),1); % compute the freq. domain kernel and pad to M
    windTimeSeries = zeros(length(t),1); % perform the IFFT in this line

    outputStruct = struct();
    outputStruct = updateStruct(outputStruct, windStruct);
    outputStruct.t = t;
    outputStruct.V_hub = windTimeSeries + windStruct.V_10;

end
