%{
%Filename: c:\Users\fabpi\OneDrive - Danmarks Tekniske Universitet\Dokumenter\Courses\46211_OffshoreWindEnergy\2024\Module3\Code\classicalSolution\Stud_\functionsMat\runner.m
%Path: c:\Users\fabpi\OneDrive - Danmarks Tekniske Universitet\Dokumenter\Courses\46211_OffshoreWindEnergy\2024\Module3\Code\classicalSolution\Stud_\functionsMat
%Created Date: Friday, October 18th 2024, 12:31:56 pm
%Author: Fabio Pierella
%
%Copyright (c) 2024 DTU Wind and Energy Systems
%}

function outputStruct = runEnvironmentalCondition(wind, waves, rotor, monopile, timeInfo)

    % Update the time information
    waves = updateStruct(waves, timeInfo);
    waves.t = ensureColVec(0:waves.dt:(waves.TDur-waves.dt));

    wind = updateStruct(wind, timeInfo);
    wind.t = ensureColVec(0.:wind.dt:(wind.TDur - wind.dt));

    % Wind time series
    wind = calculateKaimalSpectrum(wind);
    wind = generateRandomPhases(wind, wind.randomSeed);
    wind = calculateWindTimeSeriesFFT(wind);

    % Wave time series
    waves = calculateJONSWAPSpectrum(waves);
    waves = generateRandomPhases(waves, waves.randomSeed);
    waves = calculateFreeSurfaceElevationTimeSeriesFFT(waves);
    waves = calculateKinematicsFFT(waves);

    % Calculate response
    dt = timeInfo.dt;
    TDur = timeInfo.TDur;
    tIntegration = ensureColVec(0.:2*dt:(TDur - 2*dt));

    q0 = [0.; 0.];
    q = ode4(@dqdt, tIntegration, q0, monopile, ...
        rotor, waves, wind);

    % save it to a struct
    response = struct;
    response.t = tIntegration;
    response.alpha = q(:,1);
    response.alphaDot = q(:,2);
    response.alphaDotDot = gradient(response.alphaDot, tIntegration);

    % Calculate the dynamic loads
    windDownsampled = downsample(wind, 2, {"t", "V_hub"});
    windLoads = calculateStaticWindLoads(windDownsampled, rotor, ...
        monopile, response);

    % Calculate the static wind loads
    wavesDownsampled = downsample(waves, 2, {"t", "u", "ut"});
    wavesLoads = calculateStaticWaveLoads(wavesDownsampled, ...
        monopile, response);

    % Calculate the dynamic loads
    monopile = calculateElementwiseQuantities(monopile);
    dynamicLoads = calculateDynamicLoads(monopile, response);

    % save it to the final structure
    outputStruct = struct;
    outputStruct.waves = wavesLoads;
    outputStruct.wind = windLoads;
    outputStruct.dynamic = dynamicLoads;

    outputStruct.total = struct;
    outputStruct.total.t = tIntegration;
    outputStruct.total.F = outputStruct.waves.F + outputStruct.wind.F + outputStruct.dynamic.F;
    outputStruct.total.M = outputStruct.waves.M + outputStruct.wind.M + outputStruct.dynamic.M;

end