function dqdtOut = dqdt(t,q, structure, rotor, waves, wind)

    % Position and velocity
    alpha    = q(1);
    alphaDot = q(2);

    GF = GFCalc(t, alphaDot, structure, rotor, waves, wind);

    % Monopile modal acceleration
    % FIXME: implement formula for alphaDotDot as a function of GF, GD, GK, GM
    alphaDotDot = 0.;

    dqdtOut = [0.;0.];
    dqdtOut(1) = alphaDot;
    dqdtOut(2) = alphaDotDot;

end

function GF = GFCalc(t, alphaDot, structure, rotor, waves, wind)

    % Avoid re-calling constants at every function call
    persistent tol; if isempty(tol); tol = loadConstants().floatTolerance; end
    i_ = round(t/waves.dt)+1;

    % Read wind speed
    V_hub = wind.V_hub(i_);
    V_10 = wind.V_10;

    % Hub velocity
    phiHub = structure.phiNodal(end);
    % FIXME: Compute the velocity of the hub due to
    % structural deformation.
    x_dot_rotor = 0.;

    % Wind force
    Thrust = F_wind(rotor, V_10, V_hub, x_dot_rotor);
    % FIXME: add the generalized forcing from the wind
    GFWind = 0.;

    % Read wave kinematics    
    u = ensureColVec(waves.u(i_, :));
    ut = ensureColVec(waves.ut(i_, :));

    % Monopile velocity
    x_dot = ensureColVec(alphaDot*structure.phiNodal);
    wetPart = ensureColVec(structure.zBeamNodal <= 0.); 
    x_dot_submerged = x_dot(wetPart);
    z_below_water = structure.zBeamNodal(wetPart);
    phiNodalSubmerged = structure.phiNodal(wetPart);

    df = distributedForce(structure, u, ut, z_below_water, x_dot_submerged);

    % Generalized force
    % FIXME: Add the generalized forcing from the waves on top of 
    % the generalized force from the wind
    GFWaves = 0.;
    GF = GFWind + GFWaves ;
    
end

