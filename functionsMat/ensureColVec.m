function output = ensureColVec(input)
    % Makes sure that the 1D array has a column shape.
    % Avoids unwanted / nasty row times col multiplications.
    output = reshape(input, [], 1); % Ensure input is a column vector
end