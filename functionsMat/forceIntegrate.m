function [F, M] = forceIntegrate(monopileStruct, u, ut, z, x_dot)

    h = abs(z(1));

    df = distributedForce(monopileStruct, u, ut, z, x_dot);

    F = trapz(z,df);
    M = trapz(z,df.*(z+h));
    
end