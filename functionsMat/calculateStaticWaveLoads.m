function outputStruct = calculateStaticWaveLoads(waves, structure, q)

    % Calculate the structural velocity along the structural axis
    x_dot = ensureRowVec(q.alphaDot).*ensureColVec(structure.phiNodal);

    % Monopile velocity
    wetPart = ensureColVec(structure.zBeamNodal <= 0.); 
    x_dot_submerged = x_dot(wetPart);
    z_below_water = structure.zBeamNodal(wetPart);
    h = abs(z_below_water(1));

    % Initialize the arrays
    F_with_rel_motion = zeros(size(waves.t));
    M_with_rel_motion = zeros(size(waves.t));

    for i_ = 1:length(waves.t)
        u = ensureColVec(waves.u(i_, :));
        ut = ensureColVec(waves.ut(i_, :));

        % FIXME: calculate the wave distributed force using the 
        % relative velocity between the waves and the structure
        df = 0.;

        F_with_rel_motion(i_) = trapz(df, z_below_water);
        M_with_rel_motion(i_) = trapz(df.*(z_below_water+h), z_below_water);

    end

    outputStruct = struct;
    outputStruct.t = waves.t;
    outputStruct.F = F_with_rel_motion;
    outputStruct.M = M_with_rel_motion;

end