function output = ensureRowVec(input)
    % Makes sure that the 1D array has a row shape.
    % Avoids unwanted / nasty row times col multiplications.
    output = reshape(input, 1, []); % Ensure input is a row vector
end