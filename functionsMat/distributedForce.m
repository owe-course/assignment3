function df = distributedForce(monopileStruct, u, ut, z, x_dot)

    rho_water = loadConstants().rho_water;
    
    urel = u - x_dot;

    df = 0.5*rho_water*monopileStruct.DMonopile*monopileStruct.CD*abs(urel).*urel + rho_water*monopileStruct.DMonopile^2*pi/4*monopileStruct.CM*ut;
    
    df = ensureColVec(df); % needed for the integration - correct shape

end