function newStruct = loadFromJSON(filePath)
    fid = fopen(filePath, 'r');
    % Read the file content
    raw = fread(fid, inf);
    readStruct = char(raw');
    fclose(fid);            

    newStruct= jsondecode(readStruct);

end