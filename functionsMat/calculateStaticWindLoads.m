function outputStruct = calculateStaticWindLoads(wind, rotor, structure, q)

    % calculate the structural velocity at hub height
    phiHub = structure.phiNodal(end);
    x_dot_hub = phiHub*q.alphaDot;

    F_with_rel_motion = zeros(length(wind.t),1);

    for i_ = 1:length(wind.t)
        % FIXME: calculate the wind force using the 
        % relative velocity between the waves and the structure
        F_with_rel_motion(i_) = 0.;
    end

    z = structure.zBeamNodal;
    h = abs(z(1));
    zHub = z(end);

    outputStruct = struct;
    outputStruct.t = wind.t;
    outputStruct.F = F_with_rel_motion;
    outputStruct.M = F_with_rel_motion*(zHub+h);

end