function outputStruct = calculateJONSWAPSpectrum(inputStruct)
    Hs = inputStruct.Hs;
    Tp = inputStruct.Tp;

    % Spectral peakedness parameter
    if isfield(inputStruct, "gamma") 
        gamma = inputStruct.gamma;
    else
        gamma = 1.0;
    end
       
    df = inputStruct.TDur^-1.0;
    f = ensureColVec(df:df:inputStruct.fHighCut-df);

    sigma = ones(length(f),1); % row vector by design
    fp = 1/inputStruct.Tp;

    sigma(f>fp) = 0.09;
    sigma(f<=fp) = 0.07;

    powerFac=exp(-0.5*((f./fp-1)./sigma).^2);

    % FIXME: progarm the correct spectrum
    S= 1.0*ones(length(f),1);
    a = sqrt(2*S*df);

    outputStruct = struct();
    outputStruct = updateStruct(outputStruct, inputStruct);
    outputStruct.Spectrum = S;
    outputStruct.amplitudeSpectrum = a;
    outputStruct.f = f;

end