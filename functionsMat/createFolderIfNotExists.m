function createFolderIfNotExists(folderName)
    % This function creates a folder if it does not already exist.
    if ~exist(folderName, 'dir')
        mkdir(folderName);
    end
end