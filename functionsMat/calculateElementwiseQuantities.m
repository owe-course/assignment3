function outputStruct = calculateElementwiseQuantities(inputStruct)

    outputStruct = struct;
    outputStruct = updateStruct(outputStruct, inputStruct);

    % Compute missing element properties
    z = inputStruct.zBeamNodal;
    dz = diff(z);
    outputStruct.zBeamElement = z(1:end-1) + dz/2;
    outputStruct.dz = dz;

    % Compute the phiNodal
    phi = inputStruct.phiNodal;
    dPhi = diff(phi);
    outputStruct.phiElement = outputStruct.phiNodal(1:end-1) + dPhi/2;

end