function outputStruct = calculateFreeSurfaceElevationTimeSeriesFFT(inputStruct)
    
    t = inputStruct.t;
    f = inputStruct.f;
    
    omega = f*2*pi;

    % FIXME: compute the fft kernel and perform the IFFT
    M = length(t);
    etaHat = zeros(length(t),1); % compute the freq. domain kernel and pad to M
    freeSurfTimeSeries = zeros(length(t),1); % perform the IFFT in this line

    outputStruct = struct();
    outputStruct = updateStruct(outputStruct, inputStruct);

    outputStruct.t = t;
    outputStruct.eta = freeSurfTimeSeries;

end