function outputStruct = downsample(inputStruct, dropEvery, listOfFields)

    % Create the output dictionary
    outputStruct = struct;
    outputStruct = updateStruct(outputStruct, inputStruct);

    % Reduce the nr of samples in the desired field
    for iField = 1:numel(listOfFields)
        originalField = inputStruct.(listOfFields{iField});
        if isvector(originalField)
            outputStruct.(listOfFields{iField}) = originalField(1:dropEvery:end);
        else
            outputStruct.(listOfFields{iField}) = originalField(1:dropEvery:end,:);
        end
        
    end

    % Increase the time step as well
    % If you drop one every two samples, the time step doubles
    outputStruct.dt = outputStruct.dt*dropEvery;

end