function outputStruct =  calculateKaimalSpectrum(windDict)
    % Calculate time properties
    df = windDict.TDur^(-1.0);
    f = ensureColVec(df:df:(windDict.fHighCut-df));
	% FIXME: Implement kaimal spectrum
    Spectrum = ones(length(f),1);
    amplitudeSpectrum = sqrt(2*Spectrum*df);

    % Compute the spectrum
    outputStruct = struct();
    outputStruct = updateStruct(outputStruct, windDict);
    outputStruct.Spectrum = Spectrum;
    outputStruct.amplitudeSpectrum = amplitudeSpectrum;
    outputStruct.f = f;

end