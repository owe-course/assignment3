function saveToJSON(inputDictionary, filePath)
    % Convert the data structure to a JSON string
    jsonString = jsonencode(inputDictionary);
    % Write the JSON string to a file
    fid = fopen(filePath, 'w');
    fwrite(fid, jsonString, 'char');
    fclose(fid);
end